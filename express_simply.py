#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 14 14:20:35 2018

@author: Alexander Pitchford

Make subsitutions in the overall expression found as text.
The sub-expressions are any bracketed group, including nested bracketed groups.
A new file is generated with the substitutions defined in the order that
the sub-expressions were found (closing bracket).
The orginal expression is added at the end of the file.

The substitutions are then made, forward only, in the sub-expressions
and main expression.

Any sub expression that is unique (excluding whitespace) is stored in the
dictionary.

Note that all bracketed expressions are substituted with enclosing brackets,
as they may be function brackets. This leads to many extraneous brackets.
This could be avoided by recognising when an expression function params,
but would require more development, but can probably be done by looking
for an opening that is a function using a regex.

There can also be expressions that are completely subsumed by others.
These do not need to be defined as vars. Again some more development could
probably deal with this quite easily.

Attributes
----------
cfgfilename : str
    File that contains three that are
    working directory
    input file name
    output file name

repvarname : str
    Base name for the substitute variables

verbosity : int
    Messaging level. 0 for summary messages only.
    Other levels (up to 4) for various levels of debug messaging

n_occur_subs_min : int
    Minimum number of occurrences of an expression for a substitution to be
    made.

subslist : list
    List of explicit substitutions to be made in the expression before
    the simplification is made. Items should be of the form
    `(pattern, repl)` to be used in `re.sub`.
"""
import sys, os
import collections
import re

# Read the configuration from the cfg file
cfgfilename = "express_simply-cfg.dat"
verbosity = 0
repvarname = 'v'
n_occur_subs_min = 2
#subslist = [(r"E\*\*(\w+)", r"np.exp(\1)"), (r"E\*\*(\()", r"np.exp\1")]
subslist = []

#cfgfilename = "express_simply_cfg-ncl_exponential.dat"
cfgstg = "open file"
try:
    fcfg = open(cfgfilename, 'r')
    cfgstg = "read working directory from line 1"
    workdir = fcfg.readline()
    if len(workdir) == 0 or workdir == '\n':
        raise ValueError("No working directory specified")
    workdir = workdir.strip()
    if "~" in workdir:
        workdir = os.path.expanduser(workdir)
    if not os.path.isdir(workdir):
        raise ValueError("Working directory:\n'{}'"
                         "\nis not valid".format(workdir))
    workdir = os.path.normpath(workdir)
    print("Using workdir:\n{}\n".format(workdir))

    cfgstg = "read input file name line 2"
    infile = fcfg.readline()
    if len(infile) == 0 or infile == '\n':
        raise ValueError("No infile specified")
    infile = infile.strip()
    if "~" in infile:
        infile = os.path.expanduser(infile)
    if os.path.isabs(infile):
        infilepath = infile
    else:
        infilepath = os.path.join(workdir, infile)
    if not os.path.isfile(infilepath):
        raise ValueError("infile:\n'{}'\n"
                         "is not valid.".format(infile))
    infilepath = os.path.normpath(infilepath)
    print("Working on infile:\n{}\n".format(infilepath))

    cfgstg = "read output file name line 3"
    outfile = fcfg.readline()
    if len(outfile) == 0 or outfile == '\n':
        raise ValueError("No outfile specified")
    outfile = outfile.strip()
    if "~" in outfile:
        outfile = os.path.expanduser(outfile)
    if os.path.isabs(outfile):
        outfilepath = outfile
    else:
        outfilepath = os.path.join(workdir, outfile)
    outfilepath = os.path.normpath(outfilepath)
    print("Will output to outfile:\n{}\n".format(outfilepath))

    cfgstg = "read stripped file name line 4"
    strippedfile = fcfg.readline()
    if len(strippedfile) == 0 or strippedfile == '\n':
        print("No stripped file specified.\n")
        strippedfilepath = None
    else:
        strippedfile = strippedfile.strip()
        if "~" in strippedfile:
            strippedfile = os.path.expanduser(strippedfile)
        if os.path.isabs(strippedfile):
            strippedfilepath = strippedfile
        else:
            strippedfilepath = os.path.join(workdir, strippedfile)
        strippedfilepath = os.path.normpath(strippedfilepath)
        print("Will output to stripped file to:\n{}\n"
              "".format(strippedfilepath))

except Exception as e:
    #print(e)
    raise RuntimeError("Unable to successfully read cfg from file: {}, "
                       "as: {}"
                       "\nWas trying to "
                       "{}.".format(cfgfilename, e, cfgstg)) from e
fcfg.close()

operchars = "*/+-%<>="
opennestchars = "[("

# Number of time sub-expression is found in main expression
ecount = collections.Counter()

# The unique expressions.
# The key is the expression with whitespace removed.
# The values are the (whitespace retained) variants found
expressions = dict()

# List of all the expression keys in the order that they were encountered.
ekeylist = []

report_num = 100

def printv(s, v=1):
    if verbosity >= v:
        print(s)

text = None
stippedlines = []

with open(infilepath, 'r') as fin:

    exp_line_n = 0
    i = 0
    for l in fin:
        i += 1
        if len(l) > 1000:
            exp_line_n = i
            text = l
            print("Searching (first 100 chars):\n{}".format(text[:100]))
        elif strippedfilepath is not None:
            stippedlines.append(l)

if len(stippedlines) > 0:
    with open(strippedfilepath, 'w') as f:
        f.writelines(stippedlines)

if text is None:
    text = l
    print("Searching: ", text)

fin.close()

class Nest(object):
    def __init__(self, openchar):
        self.openchar = openchar
        self.text = openchar

    def extend(self, s):
        self.text += s

    def close(self, s):
        self.text += s
        if self.openchar == '(':
            self.text += ')'


class Expression(object):
    #def __init__(self, key, subs, variant):
    def __init__(self, nest):
        # expression with parenthesis
        self.expressstr = nest.text[1:-1].strip()
        # key of the expression (whitespace removed)
        self.key = self.expressstr.replace(' ', '')
        self.varname = None
        # what will be subsituted in
        self.openchar = nest.openchar
        if nest.openchar == '(':
            self.subsfmt = '({})'
            self.variants = [nest.text]
        else:
            self.subsfmt = '{}'
            self.variants = [self.expressstr]

    def subs_varname(self):
        return self.subsfmt.format(self.varname)

    def add_variants(self, e):
        if self.openchar != '(' and e .openchar == '(':
            self.subsfmt = '({})'
            self.variants = e.variants + self.variants
        else:
            self.variants += e.variants

def is_expression(expressstr):
    if any(c in expressstr for c in operchars):
        return True
    return False

def find_nest_char(s, chars, stpos):
    npos = -1
    nc = None
    for c in chars:
        printv("looking for {}".format(c), 4)
        pos = s.find(c, stpos)
        printv("found at {}".format(pos), 4)
        if pos >= 0 and (pos < npos or npos < 0):
            npos = pos
            nc = c
            printv("sel nest char {} pos {}".format(nc, npos), 3)
    return npos, nc

# Explicit substitutions
if len(subslist) > 0:
    print("Doing explicit substitutions.")
    for subparams in subslist:
        text = re.sub(subparams[0], subparams[1], text)

print("\nStarting the simplification.")
# get first open nest
onpos, onc = find_nest_char(text, opennestchars, 0)
if onpos < 0:
    raise ValueError("No expression found")
printv("first nest open at {} with {}".format(onpos, onc), 2)
nests = [Nest(onc)]
pos = onpos

while pos >= 0 and len(nests) > 0:
    if nests[-1].openchar == '(':
        closenestchars = ')'
    else:
        closenestchars = ',]'
    #onpos = text.find('(', pos+1)
    onpos, onc = find_nest_char(text, opennestchars, pos+1)
    printv("open at {} with {}".format(onpos, onc), 2)

    #cnpos = text.find(')', pos+1)
    cnpos, cnc = find_nest_char(text, closenestchars, pos+1)
    printv("close at {}  with {}".format(cnpos, cnc), 2)
    if onpos < 0 and cnpos < 0:
        break
    if onpos >= 0 and cnpos >= 0:
        nextpos = min(onpos, cnpos)
    else:
        nextpos = max(onpos, cnpos)
    snip = text[pos+1:nextpos]
    printv("snip: {}".format(snip))
    if onpos >= 0 and (onpos < cnpos or cnpos < 0):
        printv("Open nest")
        # Another open bracket occurs first
        # add this snippet to all open nests
        for i, n in enumerate(nests):
#            if len(n) > 0:
#                nests[i] += '('
            nests[i].extend(snip + onc)
        # create a new nest
        nests.append(Nest(onc))
    elif cnpos >= 0 and (cnpos < onpos or onpos < 0):
        # add the snippet to the nests
        for i, n in enumerate(nests):
            nests[i].extend(snip + cnc)
        # deepest nest is complete
        # add nest as expression

        new_e = Expression(nests[-1])
        ekey = new_e.key
        if is_expression(new_e.expressstr):

            ecount.update([ekey])
            if ekey in expressions:
                existing_e = expressions[ekey]
                if not new_e.variants[0] in existing_e.variants:
                    existing_e.add_variants(new_e)
                    printv("adding variant: {}".format(new_e.variants[0]))
            else:
                printv("adding expression: {}".format(new_e.variants[0]))
                expressions[ekey] = new_e
                ekeylist.append(ekey)
        else:
            printv("skipping: {}".format(new_e.expressstr))
        # close off
        printv("Close nest")
        del nests[-1]
        # if just closed on a comma, then open a new nest on a comma
        if cnc == ',':
            nests.append(Nest(','))

    pos = nextpos

print("\nexpressions")
n = 0
for ekey in ecount:
    if ecount[ekey] >= 9:
        n += 1

n_sube = len(ekeylist)
print("{} unique sub expressions.".format(n_sube))
print("{} used at least 9 times.".format(n))

if report_num > 0:
    #print(ecount.most_common(report_num))
    mcl = ecount.most_common(report_num)
    keys = [t[0] for t in mcl]
    mckey = mcl[0][0]

else:
    keys = expressions.keys()

print("sub expressions:")
print("key:  (count) [variants]")
for ekey in keys:
    print("'{}': ({}) {}".format(ekey, ecount[ekey],
                                 expressions[ekey].variants))

print("\nChecking for substitutions.")

def make_subs(instr, express):
    for v in express.variants:
        instr = instr.replace(v, express.subs_varname())
    return instr

subsekeys = []
i = 0
for ekey in ekeylist:
    if ecount[ekey] >= n_occur_subs_min:
        i += 1
        expressions[ekey].varname = "{}{}".format(repvarname, i)
        subsekeys.append(ekey)

if len(subsekeys) == 0:
    print("\nNo substitutions to make.")
else:
    print("\nMaking substitutions.")
    nsublinechars = 0
    subs = []
    outtext = text
    for ekey in reversed(subsekeys):
        # make substitution using the first variant of the expression
        e = expressions[ekey]
        #make subsitutions in the output text
        outtext = make_subs(outtext, e)
        #make substitions in other subs lines
        for j in range(len(subs)):
            subs[j] = (subs[j][0], make_subs(subs[j][1], e))
        #define the substitution line
        subs.insert(0, (e.varname, e.expressstr))

    fout = open(outfilepath, 'w')
    print("\nWriting the output to:\n{}\n".format(outfilepath))

    fout.write("# Subsitution vars\n")
    for s in subs:
        l = "{} = {}\n".format(s[0], s[1])
        fout.write(l)
        nsublinechars += len(l)
    fout.write("\n# Expression with subs\n")
    fout.writelines(outtext)
    fout.close()

    nnewchars = len(outtext) + nsublinechars
    propnew = nnewchars / len(text)

    print("Source expression was {} characters.\n"
          "New expression is {} characters.\n"
          "Substitutions make a total of {} characters.\n"
          "A saving of {} characters.\n"
          "New expression + subs is {:0.3f}% of source expression."
          "".format(len(text), len(outtext), nsublinechars,
                    len(text) - nnewchars, propnew*100))
